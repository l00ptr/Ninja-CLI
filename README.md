# Ninja CLI

Petite liste des trucs et astuces pour briller en société our devenir un ninja 
de la ligne de commande.

## Raccourcis clavier
Quelques raccourcis clavier utilisable pour se simplifier la vie


### Esc + .
Esc + . est équivalent à !$ qui permet de réutiliser le dernier argument de la 
commande précédente. C'est simple et éfficace si vous devez passer plusieurs
fois le même argument à des commandes différentes. 

#### Exemple

```
ls ~/bin/scripts/output
cd Esc+.
```

 - La première commande va lister le contenu du répertoire ~/bin/scripts/output
 - La seconde commande va permettre de se déplacer dans ~/bin/scripts/output, 
   **votre shell va automatiquement (et comme par magie) étendre votre commande 
   avec l'argument de la précédente**.
